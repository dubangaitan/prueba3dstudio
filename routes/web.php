<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'registerController@registerview');


Route::get('/registerview','registerController@registerview');
Route::post('registerpost', 'registerController@registerpost');
Route::get('municipioByDepar/{idDepar}', 'registerController@municipioByDepar');