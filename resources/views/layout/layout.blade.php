<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    
	    <title>Prueba Registro</title>

	    <!link href="{{ URL::asset('css/**.css') }}" rel="stylesheet">
	    
	    @yield('sectionstyle')

	</head>
	<body id="all">

		@yield('content')

	    <script src="{{ URL::asset('js/jquery/jquery-3.3.1.min.js') }}"></script>

	    @yield('script')
	        
	</body>
</html>
