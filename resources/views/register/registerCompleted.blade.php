@extends('layout.layout')


@section('sectionstyle')
@endsection

@section('content')
<div style="margin-left:5%; margin-top: 3%;">
    <h1>Success!.</h1>
    <table>
        <table class="table table-hover col-md-12" id="listnegoc">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Apellido</th>
                  <th scope="col">Cedula</th>
                  <th scope="col">telefono</th>
                  <th scope="col">Departamento</th>
                  <th scope="col">Municipio</th>
                </tr>
              </thead>
              <tbody>
                <tr>             
                  <td>{{ $register->name }}</td>
                  <td>{{ $register->surname }}</td>
                  <td>{{ $register->cc }}</td>
                  <td>{{ $register->mobilephone }}</td>
                  <td>{{ $register->depart->departamento }}</td>
                  <td>{{ $register->municip->municipio }}</td>
                </tr>

              </tbody>
            </table>
    </table>    
</div>

@endsection

@section('script')
@endsection