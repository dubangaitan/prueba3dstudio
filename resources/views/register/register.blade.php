@extends('layout.layout')


@section('sectionstyle')
	<style>
		.alert {
			color: red ;
			margin:0.5em;

		}
	</style>
	
@endsection

@section('content')
<div style="margin-left:5%; margin-top: 3%;">
	<h1>Registro</h1>
	<form method="POST" action="/registerpost" accept-charset="utf-8">
	    @csrf
	    <div style="margin:10px;">
			<label for="name" style="margin-right: 1em">Nombre</label>
			<input type="text" id="name" name="name" placeholder="Escriba el Nombre" class="@error('name') is-invalid @enderror">
			@error('name')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div style="margin:10px;">
			<label for="surname" style="margin-right:0.7em;">Apellido</label>
			<input type="text" id="surname" name="surname" placeholder="Escriba el Apellido">
			@error('surname')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div style="margin:10px;">
			<label for="cc" style="margin-right:1.4em;">Cédula</label>
			<input type="text" id="cc" name="cc" placeholder="Escriba la cédula">
			@error('cc')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div style="margin:10px;">
			<label for="mobilephone" style="margin-right:0.7em;">Telefono</label>
			<input type="text" id="mobilephone" name="mobilephone" placeholder="Escriba el Teléfono">
			@error('mobilephone')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div style="margin:10px;">
			<label for="codDepart">Departamento</label>
			<select name="codDepart" id="codDepart">
				<option value="">Seleccione</option>
				@foreach ( $departamentos as $departamento)
					<option value="{{ $departamento->id_departamento }}">{{ $departamento->departamento }}</option>
				@endforeach
				
			</select>
			@error('codDepart')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div style="margin:10px;">
			<label>Municipio</label>
			<select name="codMunicipio" id="codMunicipio">
				<option value="">Seleccione</option>
				
			</select>
			@error('codMunicipio')
			    <div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		<div class="col-sm-offset-2 col-sm-10">
        	<button type="submit" class="btn btn-default">Submit</button>
        </div>
	</form>
	
</div>

@endsection

@section('script')
<script>
	$(document).ready(function(){
		$("#codDepart").change(function(){
			var departamento = $(this).val();
			$.get('municipioByDepar/'+departamento, function(data){
				console.log(data);
				var munici = '<option value="">Seleccione</option>';
				for (var i=0; i<data.length; i++){
					munici += '<option value="'+data[i].id_municipio+'">'+data[i].municipio+'</option>'
				}
				$("#codMunicipio").html(munici);
			})
		})
	})
</script>
@endsection