<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Model\Register;
use App\Model\Municipio;
use App\Model\Departamento;

class registerController extends Controller
{
	public function registerview() //llamado de vista
	{
		$departamentos = Departamento::all();
		return view('register/register',[
			'departamentos' => $departamentos
		]);
	}
    public function registerpost(Request $request){ 
    	// Validación de campos
    	$rules = [
    		'name' => 'required|string',
    		'surname' => 'required|string',
    		'cc' => 'required|integer',
    		'mobilephone' => 'required|integer',
    		'codDepart' => 'required|integer',
    		'codMunicipio' => 'required|integer'
    	];
    	$messages = [
		    'name.required' => 'El nombre es obligatorio.',
		    'surname.required' =>'El Apellido es obligatorio',
		    'cc.required' => 'La cedula es obligatoria',
		    'cc.integer' => 'La cedula debe ser un numerica',
		    'mobilephone.required' => 'El telefono es requerido',
		    'mobilephone.integer' => 'El campo telefono va tu número de telefono/celular',
		    'codDepartd.required' => 'El Departamento es requerido',
		    'codMunicipio.required' => 'El Municipio es requerido'
		];
    	$data = $this->validate($request,$rules,$messages);
    	
    	

    	//creación de registro/ 
		$create = Register::create($data);
    	//var_dump($create);
    	return view('register/registerCompleted', [
    			'register' => $create
    	]);
    }
    public function municipioByDepar($idDepar)
    {
    	return Municipio::where('departamento_id', '=', $idDepar)->get();
    }
}
