<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class register extends Model
{
	public $timestamps = false;
	protected $fillable = [ 'name', 'surname', 'cc', 'mobilephone','codDepart','codMunicipio',];


	public function depart()
    {
        return $this->belongsTo('App\Model\Departamento','codDepart','id_departamento');
    }
    public function municip()
    {
        return $this->belongsTo('App\Model\Municipio','codMunicipio','id_municipio');
    }
}
